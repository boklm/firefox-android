/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

// Copyright (c) 2020, The Tor Project, Inc.

package org.mozilla.fenix.components

import android.content.Context
import android.graphics.Bitmap
import kotlinx.coroutines.withContext
import mozilla.components.concept.fetch.Client
import mozilla.components.feature.addons.Addon
import kotlinx.coroutines.Dispatchers
import mozilla.components.feature.addons.amo.AddonCollectionProvider
import java.io.IOException

internal const val COLLECTION_NAME = "tor_browser_collection"
internal const val ALLOWED_ADDONS_PATH = "allowed_addons.json"
internal const val MAX_CACHE_AGE = 1000L * 365L * 24L * 60L // 1000 years

class TorAddonCollectionProvider(
    private val context: Context,
    client: Client
) : AddonCollectionProvider(
    context, client, serverURL = "",
    collectionName = COLLECTION_NAME,
    maxCacheAgeInMinutes = MAX_CACHE_AGE
) {
    private var isCacheLoaded = false

    @Throws(IOException::class)
    override suspend fun getAvailableAddons(
        allowCache: Boolean,
        readTimeoutInSeconds: Long?,
        language: String?
    ): List<Addon> {
        ensureCache(language)
        return super.getAvailableAddons(true, readTimeoutInSeconds, language)
    }

    @Throws(IOException::class)
    override suspend fun getAddonIconBitmap(addon: Addon): Bitmap? {
        // super.getAddonIconBitmap does not look at the cache, so calling
        // ensureCache here is not helpful. In addition, now the cache depends
        // on a language, and that isn't available right now.
        // ensureCache(language)
        return super.getAddonIconBitmap(addon)
    }

    @Throws(IOException::class)
    private suspend fun ensureCache(language: String?) {
        if (isCacheLoaded) {
            return
        }
        return withContext(Dispatchers.IO) {
            val data = context.assets.open(ALLOWED_ADDONS_PATH).bufferedReader().use {
                it.readText()
            }
            writeToDiskCache(data, language)
            isCacheLoaded = true
        }
    }
}
