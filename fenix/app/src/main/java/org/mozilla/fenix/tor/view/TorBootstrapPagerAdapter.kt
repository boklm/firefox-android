/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.tor.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import org.mozilla.fenix.components.Components
import org.mozilla.fenix.tor.interactor.TorBootstrapInteractor

class TorBootstrapPagerAdapter(
    private val components: Components,
    private val interactor: TorBootstrapInteractor
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == BOOTSTRAP_UI_PAGE_TYPE) {
            val viewDVH = LayoutInflater.from(parent.context)
                .inflate(TorBootstrapConnectViewHolder.LAYOUT_ID, parent, false)
            return TorBootstrapConnectViewHolder(viewDVH, components, interactor)
        } else {
            val viewLVH = LayoutInflater.from(parent.context)
                .inflate(TorBootstrapLoggerViewHolder.LAYOUT_ID, parent, false)
            return TorBootstrapLoggerViewHolder(viewLVH, components)
        }
    }

    @SuppressWarnings("EmptyFunctionBlock")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
    }

    override fun getItemViewType(position: Int): Int = position

    override fun getItemCount(): Int = BOOTSTRAP_PAGE_COUNT

    companion object {
        const val BOOTSTRAP_UI_PAGE_TYPE = 0
        const val BOOTSTRAP_LOG_PAGE_TYPE = 1
        const val BOOTSTRAP_PAGE_COUNT = 2
    }
}
