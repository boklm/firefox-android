/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

package org.mozilla.fenix.tor.view

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import org.mozilla.fenix.R
import org.mozilla.fenix.components.Components
import org.mozilla.fenix.databinding.TorBootstrapPagerBinding
import org.mozilla.fenix.tor.interactor.TorBootstrapInteractor

class TorBootstrapPagerViewHolder(
        view: View,
        components: Components,
        interactor: TorBootstrapInteractor
    ) : RecyclerView.ViewHolder(view) {

    private val bootstrapPagerAdapter = TorBootstrapPagerAdapter(components, interactor)

    init {
        val binding = TorBootstrapPagerBinding.bind(view)
        binding.bootstrapPager.apply {
            adapter = bootstrapPagerAdapter
        }
    }

    companion object {
        const val LAYOUT_ID = R.layout.tor_bootstrap_pager
    }
}
